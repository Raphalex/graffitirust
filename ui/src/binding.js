/**
 * @file This file contains the `Binding` class which
 * makes the link between the UI and the Logic modules.
 */
// Requires
let ffi = require('ffi-napi')
let path = require('path')
let ref = require('ref-napi')
let fs = require('fs')


// Type definitions for binding with Rust
let ArrayType = require('ref-array-napi')
let BoolArray = ArrayType(ref.types.bool)

// Binding of the Rust library
let bind

/* Given the DEBUG option, the program will look in a different
   place for the logic binary files.
*/
let libPath = '../../logic/target/' +
  (process.env.npm_config_DEBUG ? 'debug/' : 'release/') +
  'libgraffiti'
  
// Actual binding
try {
  bind = ffi.Library(path.join(__dirname, libPath), {
    // List of the Rust functions that can be called 
    compute: ['char *', [BoolArray, 'size_t', 'size_t']]
  })
}
catch (e) {
  console.log(e)
}

class Binding {

  /**
   * @class Class used to make the Binding between the UI and Logic modules.
   */

  constructor() { }

  /**
   * Compute function that calls Rust code via an ffi.
   * 
   * This function transforms the input grid into a 
   * one-dimensional array to pass it to the actual compute function.
   * 
   * @param {Array.<Array.<bool>>} input The input grid
   * @param {number} width The width of the grid
   * @param {number} height The height of the grid
   * 
   * @throws Throws an error if the binding via ffi failed in any way.
   * 
   * @return {string} The result of the computation if it happened correctly.
   */
  compute(input, width, height) {
    if (bind != null && input != null && input.length != 0) {
    var new_array = []
    for(var i=0; i<height; i++) {
      for(var j=0; j<width; j++) {
        new_array[i*width + j] = input[i][j]
      }
    }
      var arr = new BoolArray(new_array)
      return bind.compute(arr, width, height).readCString()
    }
    else if (bind != null) {
      throw "Input is null or has no rows"
    }
    else {
      throw "Logic binding failed"
    }
  }
}

// Export the binding class so it can be used in the server
module.exports = new Binding()