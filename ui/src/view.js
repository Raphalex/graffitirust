/**
 * @file This file describes the View module.
 * It contains everything
 * related to the display and User Interface. It gets the user input
 * and passes the entered values to the Logic module.
 * 
 * @author Raphaël Colin
*/
 
//TODO Refactor this into a class

/**
 * An instance of the logic class.
 */
let logic = new Logic()

/**
 * The canvas where the used draws.
 */
let drawer = $('#drawer')

/**
 * The color picker of the page (to change drawing color).
 */
let colorPicker = $('#colorPicker')

/**
 * The place where results are displayed.
 */
let resultDisplay = $('#resultDisplay')

//TODO An error display

/**
 * @type {Object.<number, number>} An object representing the Canvas size.
 * 
 * It contains the fields `height` and `width`.
 */
let CanvasSize

/* The context of the drawer.
   Basically, every drawing function needs the 'context' object of the canvas
   instead of the canvas itself. This variable is just a convenient way to keep
   the context around instead of calling the `getContext` function all over 
   again.
*/
let ctx = drawer[0].getContext("2d")

/**
 * Boolean used to keep track of if the user is currently clicking
 * or not
 */
let clicking = false

// This is called at startup and each time the window is resized.
computeCanvasSize()

/* This is called to initialize the input grid with the right
   dimensions
*/
logic.initInput(CanvasSize)

/**
 * Function called when the user clicks on the canvas.
 * 
 * Sets the `clicking` boolean to `true`
 * 
 * @param {Object} event An event (provided by the jQuery method)
 */
function userClick(event) {
  if (event.which == 1) {
    clicking = true
    var [x, y] = mousePositionCanvas()
    ctx.beginPath()
    ctx.strokeStyle = colorPicker.val()
    ctx.moveTo(x, y)
  }
}

/* Bind the userClick function so it is called when the user clicks on
   the canvas.
*/
drawer.mousedown(userClick)

/**
 * Stop drawing and trigger the computation once the user
 * releases the mouse button.
 * 
 * Sets the `clicking` boolean to `false`
 * 
 * @param {Object} event An event (provided by the jQuery method)
 */
function userRelease(event) {
  if (event.which == 1) {
    if(clicking) {
      displayResult()
      clearInput()
    }
    clicking = false
  }
}

/* Bind the userRelease function so it is called when the user releases
   the mouse button anywhere.
*/
$(document).mouseup(userRelease)

/**
 * Update the canvas when the mouse moves.
 * 
 * Calls the `drawPixel` function
 * 
 * @param {Object} event An event (provided by the jQuery method)
 */
function userMove(event) {
  if (clicking) {
    var [relX, relY] = mousePositionCanvas()
    
    drawPixel(relX, relY)
  }
}

/* Bind the userMove function so it is called when the user moves
   the mouse on the canvas.
*/
drawer.mousemove(userMove)

/**
 * Displays the results of the computation given the `logic` object
 * 
 * Calls the `logic.compute()` function.
 */
function displayResult() {
  var res
  try {
  res = logic.compute()
  }
  catch(e){
    alert(e)
  }
  resultDisplay[0].textContent += res
}

/**
 * Computes the position of the mouse, relative to the canvas.
 * 
 * @return {Array.<number>} An array of two elements representing the 
 * X coordinate of the mouse relative to the canvas and the Y coordinate
 * of the mouse relative to the canvas.
 */
function mousePositionCanvas() {
  var offset = drawer[0].getBoundingClientRect()
        scaleX = drawer[0].width / offset.width
        scaleY = drawer[0].height / offset.height

    return [(event.pageX - offset.left) * scaleX, 
            (event.pageY - offset.top) * scaleY]
}

// Clear the canvas and refresh the canvas size on each resize of the window.
$(window).resize(function () {
  computeCanvasSize()
  clearInput()
})

/**
 * Simple function to fill the `canvasSize` object.
 */
function computeCanvasSize() {
  CanvasSize = { width: drawer.width(), height: drawer.height() }
}

/**
 * Display the drawn pixels in the canvas.
 * 
 * Display and tells the `logic` object to update its input grid.
 * 
 * @param {number} x The X coordinate of the pixel that was drawn
 * @param {number} y The Y coordinate of the pixel that was drawn
 */
function drawPixel(x, y) {
  ctx.lineTo(Math.round(x), Math.round(y))
  ctx.stroke()
  
  logic.addPoint(x, y)
  
  ctx.moveTo(Math.round(x), Math.round(y))
}

/**
 * Clear the canvas
 */
function clearInput() {
  ctx.clearRect(0, 0, drawer[0].width, drawer[0].height)
  ctx.closePath()
  
  computeCanvasSize()
  logic.initInput(CanvasSize)
}