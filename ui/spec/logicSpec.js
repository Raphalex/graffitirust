describe('Input init', function() {
  beforeEach(function() {
    this.logic = require('../src/logic')
    this.logic.initInput({width: 50, height: 50})
  })
  
  afterEach(function() {
    delete this.logic
  })
  
  it('Init size', function() {
    expect(this.logic.input.length).toEqual(50)
    expect(this.logic.input[0].length).toEqual(50)
  })

  it('Init values', function() {
    for (var i=0; i<this.logic.input.length; i++) {
      for (var j=0; j<this.logic.input[i].length; j++) {
        expect(this.logic.input[i][j]).toBe(false)
      }
    }
  })
})

describe('Input modification', function() {
  beforeEach(function() {
    this.logic = require('../src/logic')
    this.logic.initInput({width: 50, height: 50})
  })
  
  afterEach(function() {
    delete this.logic
  })
  
  it('Add to input is true', function() {
    expect(this.logic.input.length).toEqual(50)
    this.logic.addPoint(0, 0)
    expect(this.logic.input[0][0]).toBe(true)
    
    this.logic.addPoint(this.logic.input.length - 1, this.logic.input.length-1)
    expect(this.logic.input[this.logic.input.length-1]
                           [this.logic.input.length-1]).toBe(true)
    
    this.logic.addPoint(2.3, 4.6)
    expect(this.logic.input[5][2]).toBe(true)
  })
  
  it('Add to input others not true', function() {
    this.logic.addPoint(0, 0)
    for (var i=0; i<this.logic.input.length; i++) {
      for (var j=0; j<this.logic.input[i].length; j++) {
        if (i != 0 && j != 0)
          expect(this.logic.input[i][j]).toBe(false)
      }
    }
  })
})

describe('Submit input', function() {
  beforeEach(function () {
    this.logic = require('../src/logic')
    this.logic.initInput({width: 50, height: 50})
  })
})