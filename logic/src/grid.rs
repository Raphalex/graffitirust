//! # Grid
//! This module describes input and scaled grids. A grid is basically a 2 
//! dimensional vector of booleans representing on or off pixels.
use std::str;

/// Basic grid structure made of a vector of vector of booleans.
pub struct Grid {
    /// Vector of vector of booleans representing the lit up pixels (if true)
    pub values: Vec<Vec<bool>>,
}

impl Grid {
    /// Creates a new Grid from a 2 dimensional vector.
    /// 
    /// # Arguments
    /// 
    /// * `val` - The vector of vectors of booleans used to initialize the grid
    /// 
    /// # Return Value
    /// An initialized grid
    pub fn new(val: Vec<Vec<bool>>) -> Self {
        Grid {values: val}
    }
    
    /// Reduces the Grid to the smallest rectangle containing all lit up 
    /// pixels.
    pub fn crop(&mut self) {
        let mut min_x = 0;
        let mut min_y = 0;
        let mut max_x = 0;
        let mut max_y = 0;
        let mut found_mins = false;
        let size_y = self.values.len();
        for i in 0..size_y {
            let size_x = self.values[i].len();
            for j in 0..size_x {
                if self.values[i][j]{
                    if !found_mins {
                        min_y = i;
                        min_x = j;
                        found_mins = true;
                    }
                    else {
                        max_y = i;
                        max_x = j;
                    }
                }
            }
        }
        self.values = self.values.drain(min_y..(max_y + 1)).collect();
        let size = self.values.len();
        for i in 0..size {
            self.values[i] = self.values[i].drain(min_x..(max_x + 1)).collect();
        }
    }
}


/// Creates a [`Grid`] from a pointer to an array of bool.
/// 
/// # Arguments
/// 
/// * `arr_ptr` - pointer to the array
/// * `width` - width of the grid
/// * `height` - height of the grid
/// 
/// # Return value
/// 
/// A Grid made from the array pointed by `arr_ptr`
pub fn construct_grid(arr_ptr: *const bool, width: usize, height: usize) -> Grid {
    let size = width*height;
    let arr = unsafe { std::slice::from_raw_parts(arr_ptr, size) };
    let mut vec = Vec::new();
    for i in 0..height {
        let mut tmp_vec = Vec::new();
        for j in 0..width {
            tmp_vec.push(arr[j+i*width]);
        }
        vec.push(tmp_vec)
    }
    Grid::new(vec)
}


#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn reconstructs_grid_correctly() {
        let arr : [bool; 5000] = [true; 5000];
        let arr_ptr = &arr as *const bool;
        let grid: Grid = construct_grid(arr_ptr, 50, 100);
        let mut test = true;
        for i in 0..100 {
            for j in 0..50 {
                test &= grid.values[i][j];
            }
        }
        assert!(test);
        let arr : [bool; 5000] = [true; 5000];
        let arr_ptr = &arr as *const bool;
        let grid: Grid = construct_grid(arr_ptr, 100, 50);
        test = true;
        for i in 0..50 {
            for j in 0..100 {
                test &= grid.values[i][j];
            }
        }
        assert!(test);
        let arr : [bool; 286832] = [true; 286832];
        let arr_ptr = &arr as *const bool;
        let grid: Grid = construct_grid(arr_ptr, 728, 394);
        test = true;
        for i in 0..50 {
            for j in 0..100 {
                test &= grid.values[i][j];
            }
        }
        assert!(test);
    }
    
    #[test]
    fn crop_size_is_correct() {
        let mut vec : Vec<Vec<bool>> = vec![vec![false; 500]; 500];
        vec[40][40] = true;
        vec [400][478] = true;
        let mut grid = Grid::new(vec);
        grid.crop();
        assert_eq!(grid.values.len(), 361);
        assert_eq!(grid.values[0].len(), 439);
    }
    
    #[test]
    fn crop_values_are_correct() {
        let mut vec : Vec<Vec<bool>> = vec![vec![false; 500]; 500];
        vec[40][40] = true;
        vec [400][478] = true;
        let mut grid = Grid::new(vec);
        grid.crop();
        for i in 0..grid.values.len() {
            for j in 0..grid.values[i].len() {
                if (i == 0 && j == 0) || 
                (i == grid.values.len() - 1 && j== grid.values[i].len() - 1) {
                    assert!(grid.values[i][j]);
                }
                else {
                    assert_eq!(grid.values[i][j], false);
                }
            }
        }
    }
}