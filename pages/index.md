---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: default
title: Home
nav_order: 1
description: "GraffitiRust is a learning project that aims at creating a program that can recognize handwritten letters or characters."
permalink: /
---

# Welcome to the GraffitiRust project's pages website !

![logo]({{site.url}}{{site.baseurl}}/{{site.data.img_links.logo}})

## Getting started

To get started, follow the guidelines in the README file (displayed
as the front page of the [**GitLab project**]({{site.project_url}})).