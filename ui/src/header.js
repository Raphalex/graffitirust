/**
 * @file This file contains definitions and constants fo the rest of the 
 * program.
 */
 
/**
 * Port on which the server is running.
 */
const PORT = 8081

// In case we run in a desktop environment, we need to load JQuery appropriately
if(typeof(process) != 'undefined') {
  var $ = require('jquery')
  module.exports = {
    PORT: PORT
  }
}