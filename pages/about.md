---
layout: page
title: About
nav_order: 4
permalink: /about/
---

This is the pages site for the 
[**GraffitiRust**]({{site.project_url}}) learning project. It contains
documentation and other useful things about the project.