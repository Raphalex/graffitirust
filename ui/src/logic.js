/**
 * @file This file describes the Logic module which treats the grid entered
 * (via the View module) by the user. This module is in fact, only used
 * on the client side (it runs in the browser). It treats and formats
 * the input to send it to the server via HTTP, and gives the result
 * back to the View module. It's pretty small and doesn't do the 
 * actual calculations.
 * 
 * @author Raphaël Colin
 */

/* This snippet is used to determine if the code is running in
   desktop mode (Node) or in browser.
   Here, if we're in desktop mode, we need to require the header.js
   file (which contains different constants and definitions).
   This header is also included in browser but in a different way,
   since require isn't supported in browsers.
*/

//TODO Change the way urls are defined that does not involve hard coding them.
if (typeof(process) != 'undefined') {
  var header = require('./header')
  /* For the moment, the default address in desktop mode
     is local to run tests
  */
  var url = "http://127.0.0.1:" + header.PORT
}
else {
  /* In browser mode, the default address is the remote
     one
  */
  var url = "http://raphi.achencraft.fr/graffitirust"
}

/**
 * This function changes the url of the server to localhost.
 * 
 * This is meant to be used in the browser (or electron app) developer console
 * in order to debug and use a local server (when the remote server isn't 
 * deployed) or shouldn't be messed with
 * 
 * @return {string} Just a string to give feedback in the console
 */
function use_local_server() {
  url = "http://127.0.0.1:" + PORT
  return "Set to use local server"
}

class Logic{

  /**
   * @class Class that contains everything related to the user input grid.
  */
  constructor(){
    /**
     * @property {bool[][]} input A 2 dimensional array containing boolean values
     * (true means the user lit up this pixel, false means they didn't)
     */
    this.input = []
  }

  /**
   * Initialize the input to the right size and the right values.
   * 
   * An empty input contains only the value `false`.
   * 
   * @param {Object.<number, number>} CanvasSize An object with the fields height and width, 
   * representing the height and width of the input canvas.
   
   */
  initInput(CanvasSize) {
    this.input = []
    for(var i=0; i<CanvasSize.height; i++) {
      this.input[i] = []
      for(var j=0; j<CanvasSize.width; j++) {
        this.input[i][j] = false
      }
    }
    this.height = CanvasSize.height
    this.width = CanvasSize.width
  }

  /**
   * Add a point to the input grid (changes the value to `true`)
   * 
   * @param {number} x The column of the cell
   * @param {number} y The line of the cell
   * 
   */
  addPoint(x, y) {
    this.input[Math.round(y)][Math.round(x)] = true
  }
  
  /**
   * Format the input grid to send it via HTTP.
   * 
   * @return {string} The formatted string
   */
  formatInput() {
    let res = "";
    for(var i=0; i<CanvasSize.height; i++) {
      for(var j=0; j<CanvasSize.width; j++) {
        if (this.input[i][j]) {
          res += "1"
        }
        else {
          res += "0"
        }
      }
      res +=";"
    }
    return res
  }
  
  /**
   * Compute the result of the computation of the input grid.
   * 
   * Actually sends the input via HTTP and returns the response of the server.
   * 
   * @return {string} The result
   */
  compute() {
  let response
    $.ajax({
      url : url + "/compute",
      type: "POST",
      data: "input=" + this.formatInput(),
      async: false,
      success: function(result) {
        response = result
      },
      error: function(error) {
        // TODO: Handle error correctly
        response = "ça marche pas"
      }
    })
    return response
  }
}

/* This snippet is used so the tests work properly.
   The best way to include files in other files is to
   export the content in module.exports, which is only
   possible when running in the Node environnement 
   (meaning, not in a browser). The process object 
   doesn't exist in browser JavaScript, so this 
   code will only run in a Node environnement.
*/

if (typeof(process) != 'undefined') {
  module.exports = new Logic()
}