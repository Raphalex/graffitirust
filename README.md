# GraffitiRust

Rust version of a C++ program made to recognize Graffiti letters. Uses Electron for the UI.

Learning project

## Installation

Make sure you have `node` and `npm` installed, to check, run: 

```sh
npm --version
```

and

```sh
node --version
```

also make sure you have `cargo` installed to compile the Rust code :

```sh
cargo --version
```

if you get the version number, you're all set!

if you don't, run :

```sh
curl -sL https://deb.nodesource.com/setup_13.x | sudo -E bash -
apt-get install nodejs
```

and

```sh
sudo apt-get install cargo
```

To install dependencies for the UI, run 

```sh
cd ui
npm install
```

## Running the UI

To run only the UI without the logic behind it, go to the `ui` directory and run :

```sh
npm start
```

## Building the logic

To build the logic part written in Rust (the part that actually computes the results), go to the `logic` directory and run :

```sh
cargo build
```

Be careful! If you run only `cargo build`, you will compile a debug version and will need to run the UI with :

```sh
npm start --debug=true
```
for it to work.

To build a release version of the Rust code (with optimizations), run :

```sh
cargo build --release
```
instead.

## Running tests

### UI

To run the unit tests for the UI, go to the `ui` directory and run :

```sh
npm test
```

### Logic

To Run the unit tests for the logic, go to the `logic` directory and run :

```sh
cargo test
```

## Documentation

The documentation of the project is available on this project's GitLab pages :
[Check out the doc](https://raphalex.gitlab.io/graffitirust)
