//! # Graffiti
//! 
//! This library is used to compute characters from drawings. The functions in
//! this crate are the most general and high-level functions, and are the ones
//! that should be used for interactions with the user interface.




extern crate libc;
use libc::c_char;
use std::ffi::CString;
use std::env;

pub mod grid;
pub mod scaled_grid;
pub mod character;

/// Recognizes a letter from an (unsafe) 2 dimensional array of booleans 
/// representing lit up pixels in a grid.
/// 
/// # Arguments
///
/// * `arr_ptr` - Pointer to the array of booleans
/// * `width` - Width of the grid
/// * `height` - Height of the grid
///
/// # Return value
/// 
/// A mutable c-style string representing the recognized letter (or string)
/// 
/// # Remarks
/// 
/// This function should never be used directly, but should be called from
/// the UI (JavaScript).
#[no_mangle]
pub extern fn compute(arr_ptr: *const bool, width: usize, height: usize) ->
*mut c_char {
    let db_path_res = env::var("GRAFFITI_DB_PATH");
    let db_path = match db_path_res {
        Ok(a) => a,
        Err(_) => return transform_to_c_str("GRAFFITI_DB_PATH not set".to_string())
    };
    let input_grid: grid::Grid = grid::construct_grid(arr_ptr, width, height);
    
    let result =  String::from("NotImplementedYet");
    transform_to_c_str(result)
}

fn transform_to_c_str(input: String) -> *mut c_char {
    let c_str = CString::new(input).unwrap();
    c_str.into_raw()
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[ignore]
    fn compute_works() {
        let arr : [bool; 5000] = [true; 5000];
        let arr_ptr = &arr as *const bool;
        let grid: grid::Grid = grid::construct_grid(arr_ptr, 50, 100);
        let mut test = true;
    }
}