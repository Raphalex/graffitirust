
/**
 * @file This file contains the code that run server-side.
 */
 
let express = require('express');
let app = express()
var bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ limit: '1mb', extended: true}))
app.use(bodyParser.json({limit: '1mb'}))

/**
 * Contains the definitions that are in the header file.
 */
let header = require('./header')
/**
 * Binding object used to call the `Logic` functions.
 */
let binding = require('./binding')

/**
 * Parses the input grid received via HTTP.
 * 
 * @param {string} text The input grid received as a string
 * 
 * @return {Array.<Array.<bool>>} The input grid compiled into a two-dimensional
 * array.
 */
function parseInput(text) {
  let input = []
  input.push([])
  var line = 0
  var column = 0
  for(var i=0; i<text.length; i++){
    if(text[i] == ';') {
      input.push([])
      line++
      column = 0
    }
    else {
      if (text[i] == '1') {
        input[line][column] = true
      }
      else {
        input[line][column] = false
      }
      column++
    }
  }
  return input
}

// Define the behavior when using the REST API on /compute
app.post('/compute', function (req, res) {
    let input = parseInput(req.body.input)
    res.setHeader("Access-Control-Allow-Origin", "*")
    res.end(binding.compute(input, input[0].length, input.length))
//  res.end(req.body.input)
})

// Initialize the server.
var server = app.listen(header.PORT, function() {
  var host = server.address().address
  var port = server.address().port
  if (host == '::') {
    host = '127.0.0.1'
  }
  console.log('Server listening on http://' + host + ':' + port)
})