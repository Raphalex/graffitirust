//! # ScaledGrid
//!
//! A grid that is scaled to a known size. Since the size is known, scaled grids
//! can be compared against each other and can be stored.
use crate::grid::Grid;

/// Default height for a ScaledGrid
pub const SIZE_X: usize = 400;
/// Default width for a ScaledGrid
pub const SIZE_Y: usize = 400;

/// ScaledGrid structure containing the values of the grid 
/// (see [`Grid`](../grid/struct.Grid.html))
#[derive(Debug)]
pub struct ScaledGrid {
    /// see [`Grid`](../grid/struct.Grid.html)
    pub values: Vec<Vec<bool>>,
    /// Height of the grid
    size_x: usize,
    /// Width of the grid
    size_y: usize,
}

impl ScaledGrid {
    /// Creates a new ScaledGrid from a 2 dimensional vector of booleans
    /// 
    /// # Arguments
    ///
    /// * `val` - The input vector, it must have a size of [SIZE_Y] by [SIZE_X]
    /// 
    /// # Return value
    /// 
    /// A ScaledGrid made from `val`, and of size [SIZE_Y] by [SIZE_X]
    ///
    /// [SIZE_Y]: constant.SIZE_Y.html 
    /// [SIZE_X]: constant.SIZE_X.html
    pub fn new(val: Vec<Vec<bool>>) -> Self {
        let sg = ScaledGrid {values: val, size_x: SIZE_X, size_y: SIZE_Y};
        if sg.values.len() != sg.size_y {
            panic!((format!("Height does not respect fixed size ({})", sg.size_y)));
        }
        if sg.values.len() != 0 && sg.values[0].len() != sg.size_x {
            panic!(format!("Width does not respect fixed size ({})", sg.size_x));
        }
        sg
    }
    
    /// Returns the ScaledGrid in the form of a string.
    /// 
    /// # Return value
    /// 
    /// The ScaledGrid in the form of a string :
    /// true values are written with a `1` and false with a `0`. The `\n` 
    /// character splits each line.
    pub fn as_string(&self) -> String {
        let mut res = String::new();
        for i in 0..self.size_y {
            for j in 0..self.size_x {
                res +=  if self.values[i][j] 
                            { &"1" } 
                        else 
                            { &"0" };
            }
            res += &"\n";
        }
        res
    }
}

impl From<&mut Grid> for ScaledGrid {
    
    /// Create a ScaledGrid from a regular [Grid] (../grid/struct.Grid.html)
    /// 
    /// # Arguments
    /// 
    /// * `grid` - The Grid to use as base for the conversion
    /// 
    /// # Return value
    /// 
    /// The resulting ScaledGrid
    fn from(grid: &mut Grid) -> Self {
        grid.crop();
        if grid.values.len() == 0 || grid.values[0].len() == 0 {
            panic!("Input grid has invalid dimensions (width or height is null");
        }
        let grid_size_y = grid.values.len();
        let grid_size_x = grid.values[0].len();
        let ratio_y = grid_size_y as f64 / SIZE_Y as f64;
        let ratio_x = grid_size_x as f64 / SIZE_X as f64;
        
        let mut vec = Vec::new();
        for i in 0..SIZE_Y {
            vec.push(Vec::new());
            vec[i].resize(SIZE_X, false);
        }
        for i in 0..grid_size_y {
            for j in 0..grid_size_x {
                if grid.values[i][j] {
                    vec[(i as f64/ratio_y) as usize]
                    [(j as f64/ratio_x) as usize] = true;
                }
            }
        }
        ScaledGrid::new(vec)
    }
}


#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  #[should_panic(expected = "does not respect fixed size")]
  fn scaled_grid_size_restrictions_error() {
      let v = vec![vec![true,false,true], vec![false, true, false]];
      ScaledGrid::new(v);
  }
  
  #[test]
  fn scaled_grid_size_restrictions_ok() {
      let mut v = Vec::new();
      for i in 0..SIZE_Y {
          let tmp = Vec::new();
          v.push(tmp);
          for j in 0..SIZE_X {
              v[i].push(true);
            }
        }
      ScaledGrid::new(v);
    }
  
  #[test]
  fn converting_from_grid_same_size_is_correct() {
        let mut vec : Vec<Vec<bool>> = vec![vec![false; 400]; 400];
        vec[0][0] = true;
        vec [399][399] = true;
        let mut grid = Grid::new(vec);
        let scaled_grid = ScaledGrid::from(&mut grid);
        for i in 0..scaled_grid.values.len() {
            for j in 0..scaled_grid.values[i].len() {
                if (i == 0 && j == 0) || (i == 399 && j == 399) {
                    assert!(scaled_grid.values[i][j]);
                }
                else {
                    assert!(!scaled_grid.values[i][j]);
                }
            }
        }
    }
    
    #[test]
    fn converting_from_grid_larger_is_correct() {
    let mut vec : Vec<Vec<bool>> = vec![vec![false; 800]; 800];
    vec[0][0] = true;
    vec [799][799] = true;
    let mut grid = Grid::new(vec);
    let scaled_grid = ScaledGrid::from(&mut grid);   
    
  }
}