---
nav_order: 3
title: Project Architecture
layout: default
---

# {{page.title}}

This section contains diagrams and explanations on the project architecture.
It contains general diagrams as well as more detailed diagrams on specific components.

## General Architecture

The following diagram describes the general architecture of the project.

<embed src="{{site.url}}{{site.baseurl}}/{{site.data.diagram_links.general}}" style="width: 60vw; height: 100vh"/>
