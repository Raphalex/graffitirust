# Contribute

## How to contribute to this project

To contribute, the first step is the read the README.md, fork this repository and follow the installation instructions.

Use the documentation to learn about the project architecture before making changes.

Once you're familiar with the project architecture, pick an issue and implement
the feature or the bug it's about, or create a new one.

From this issue, create a branch from develop on which you will implement the 
feature or bug fix.

When you're done (and when the pipeline is successful), create a merge request.
We will discuss it and review the code, once everything is set, your branch will
be merged !
