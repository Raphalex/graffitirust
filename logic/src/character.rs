//! # Character
//! This module defines a character (letter to recognize) as seen by the 
//! library. A character is a string associated with a list of 
//! [ScaledGrid](../scaled_grid/struct.ScaledGrid.html) representing a pool of 
//! handwritten representations of the character.
//!
//! The module includes a structure to manage graphs of Characters (since some 
//! characters, like ones that are drawns in multiple strokes, might have 
//! parent - child relationships with other characters :
//! The `A` letter for exemple is made of an horizontal bar and an upside-down 
//! `V`. This means we can code it with 2 Characters : the Character 
//! `horizontal-bar` and the Character `A`, with `A` being the child of
//! `horizontal-bar`.
//! In code, it might look like this : 
//!
//! ```
//! let h = Character::new(String::from("horizontal-bar"));
//! let a = Character::new(String::from("A"));
//! let cg = CharacterGraph::new();
//! cg.add_ch(h); // h has id 0 (it was added first)
//! cg.add_ch(a); // a has id 1 (it was added in second)
//! cg.add_relation(0, 1) // 0 (horizontal-bar) is parent of 1 (A)
//! ```
use crate::scaled_grid::ScaledGrid;
use std::cell::RefCell;
use std::rc::Rc;


/// Zone enum representing a corner (or border) of a grid.
#[derive(Debug, PartialEq)]
pub enum Zone {
  Top,
  Bottom,
  Right,
  Left,
  TopLeft,
  TopRight,
  BottomLeft,
  BottomRight,
  Any
}

/// A structure presenting a character (in the library's semantic).
#[derive(Debug)]
pub struct Character {
  /// The actual string representing the Character
  pub ch: String,
  /// A list of [ScaledGrid] (../scaled_grid/struct.ScaledGrid.html)
  pub sgv: Vec<ScaledGrid>,
  /// The id of the Character if it's in a 
  /// [CharacterGraph](struct.CharacterGraph.html)
  pub id: usize,
  /// The starting point of the drawn character (which corner / border does the 
  /// stroke start on ?)
  pub stz: Zone
}

impl Character {

  /// Initializes a Character from a string
  /// 
  /// # Arguments
  /// 
  /// * `ch` - The string that represents the Character
  /// 
  /// # Return value
  /// 
  /// An initialized Character
  pub fn new(ch: String) -> Self {
    let mut res : Character = Default::default();
    res.ch = ch;
    res
  }
  
  /// Load a list of [ScaledGrid] (../scaled_grid/struct.ScaledGrid.html) into 
  /// this Character.
  /// 
  /// # Arguments
  /// 
  /// * `vec` - The actual vector to load
  pub fn load_vec(&mut self, vec: Vec<ScaledGrid>) {
    self.sgv = vec;
  }
  
  /// Set the starting zone of the stroke of the Character.
  /// 
  /// # Arguments
  /// 
  /// * `z` - The [Zone] (enum.Zone.html) to pass to the Character
  pub fn set_zone(&mut self, z: Zone) {
    self.stz = z;
  }
}

impl Default for Character {
  /// Create a default Character.
  /// 
  /// # Return value
  /// 
  /// A Character with default fields
  fn default() -> Self {
    Character{ch: String::from(""),
              sgv: Vec::new(),
              id: 0,
              stz: Zone::Any
              }
  }
}

impl PartialEq for Character {
  /// Compare a Character against another.
  /// 
  /// # Arguments
  /// 
  /// * `other` - The Character to compare this Character against
  /// 
  /// # Return value
  /// 
  /// A boolean that is true is `self == other` and false otherwise
  fn eq(&self, other: &Self) -> bool {
    self.ch == other.ch
  }
}

impl Eq for Character {}

/// The maximum number of [Character] (struct.Character.html) in a 
/// [CharacterGraph] (struct.CharacterGraph.html).
const TABLE_SIZE: usize = 1000;

/// A structure representing a graph of Characters.
pub struct CharacterGraph {
  table: [[bool; TABLE_SIZE];TABLE_SIZE],
  list: Vec<Character>
}

impl CharacterGraph {

  /// Create a new CharacterGraph
  pub fn new() -> Self {
    Default::default()
  }
  
  /// Add a Character to this CharacterGraph.
  /// 
  /// # Arguments
  /// 
  /// * `c` - The Character to add to the CharacterGraph
  pub fn add_ch(&mut self, mut c: Character) {
    c.id = self.list.len();
    self.list.push(c);
  }
  
  /// Add a parent - child relation to the CharacterGraph.
  /// 
  /// # Arguments
  /// 
  /// * `p` - The parent Character
  /// * `s` - The child Character
  pub fn add_relation(&mut self, p: usize, s: usize) {
    if self.list.len() <= p || self.list.len() <= s {
      panic!("Index is too large for list size");
    }
    self.table[p][s] = true;
  }
  
  /// Get a Character from the CharacterGraph.
  /// 
  /// # Arguments
  /// 
  /// * `id` - The id of the Character in the CharacterGraph
  /// 
  /// # Return value
  /// 
  /// The Character with the `id` passed to the function
  /// 
  /// # Panics
  /// 
  /// This function panics if no character with such an id exists in the 
  /// CharacterGraph
  pub fn get(&mut self, id: usize) -> &mut Character {
    self.list.get_mut(id).unwrap()
  }
  
  /// Check if a Character has a successor (or child).
  /// 
  /// # Arguments
  /// 
  /// * `id` - id of the Character to be checked
  /// 
  /// # Return value
  /// 
  /// A boolean being true if the Character has at least a child and false 
  /// otherwise
  pub fn has_succ(&self, id: usize) -> bool {
    for i in 0..TABLE_SIZE {
      if self.table[id][i] {
        return true
      }
    }
    false
  }
  
  /// Check if a Character has a predecessor (or parent).
  /// 
  /// # Arguments
  /// 
  /// * `id` - id of the Character to be checked
  /// 
  /// # Return value
  /// 
  /// A boolean being true if the Character has at least a parent and false 
  /// otherwise
  pub fn has_pred(&self, id: usize) -> bool {
    for i in 0..TABLE_SIZE {
      if self.table[i][id] {
        return true
      }
    }
    false
  }
  
  /// Get the successors of a Character.
  /// 
  /// # Arguments
  /// 
  /// * `id` - id of the Character to retrieve the successors from
  /// 
  /// # Return value
  /// 
  /// A vector filled with the ids of the Characters that are successors of the 
  /// one passed in the function
  pub fn get_succs(&self, id: usize) -> Vec<usize> {
    let mut res = Vec::new();
    for i in 0..TABLE_SIZE {
      if self.table[id][i] {
        res.push(i);
      }
    }
    res
  }
  
  /// Get the predecessors of a Character.
  /// 
  /// # Arguments
  /// 
  /// * `id` - id of the Character to retrieve the predecessors from
  /// 
  /// # Return value
  /// 
  /// A vector filled with the ids of the Characters that are predecessors of 
  /// the one passes in the function
  pub fn get_preds(&self, id: usize) -> Vec<usize> {
    let mut res = Vec::new();
    for i in 0..TABLE_SIZE {
      if self.table[i][id] {
        res.push(i);
      }
    }
    res
  }
}

impl Default for CharacterGraph {
  
  /// Create a defaults CharacterGraph.
  /// 
  /// # Return value
  /// 
  /// A CharacterGraph with default values
  fn default() -> Self {
    CharacterGraph {table: [[false; TABLE_SIZE];TABLE_SIZE],
                    list: Vec::new()
                    }
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn new_character() {
    let c = Character::new("test".to_string());
    assert_eq!(c.ch, "test".to_string());
    assert_eq!(c.sgv.len(), 0);
  }
  
  #[test]
  fn load_vec() {
    let mut g1 = crate::grid::Grid::new(vec![vec![true,false,true]]);
    let mut g2 = crate::grid::Grid::new(vec![vec![true,false,true]]);
    let vec = vec![ScaledGrid::from(&mut g1), ScaledGrid::from(&mut g2)];
    let mut c = Character::new("test".to_string());
    c.load_vec(vec);
    assert_eq!(c.sgv.len(), 2);
  }
  
  #[test]
  fn set_zone() {
    let mut c1 = Character::new(String::from("test"));
    c1.set_zone(Zone::Right);
    assert_eq!(c1.stz, Zone::Right);
  }
  
  #[test]
  fn new_character_graph() {
    let cg = CharacterGraph::new();
    let mut test = true;
    for i in 0..TABLE_SIZE {
      for j in 0..TABLE_SIZE {
        test &= !cg.table[i][j];
      }
    }
    assert!(test);
    assert_eq!(cg.list.len(), 0);
  }
  
  #[test]
  fn add_character_to_graph() {
    let mut cg = CharacterGraph::new();
    let mut c = Character::new(String::from("test"));
    let mut c2 = Character::new(String::from("test2"));
    cg.add_ch(c);
    cg.add_ch(c2);
    assert_eq!(cg.list[0].ch, String::from("test"));
    assert_eq!(cg.list[0].id, 0);
    assert_eq!(cg.list[1].ch, String::from("test2"));
    assert_eq!(cg.list[1].id, 1);
  }
  
  #[test]
  fn add_relation() {
    let mut cg = CharacterGraph::new();
    let mut c = Character::new(String::from("test"));
    let mut c2 = Character::new(String::from("test2"));
    cg.add_ch(c);
    cg.add_ch(c2);
    cg.add_relation(0, 1);
    assert!(cg.table[0][1]);
  }

  #[test]
  #[should_panic(expected = "Index is too large")]
  fn add_relation_panic() {
    let mut cg = CharacterGraph::new();
    cg.add_relation(0, 1);
  }
  
  #[test]
  fn get_from_character_graph() {
    let mut cg = CharacterGraph::new();
    let mut c = Character::new(String::from("test"));
    cg.add_ch(c);
    assert_eq!(cg.get(0).ch, String::from("test"));
  }
  
  #[test]
  fn has_succ() {
    let mut cg = CharacterGraph::new();
    let mut c = Character::new(String::from("test"));
    let mut c2 = Character::new(String::from("test2"));
    cg.add_ch(c);
    cg.add_ch(c2);
    assert!(!cg.has_succ(0));
    cg.add_relation(0, 1);
    assert!(cg.has_succ(0));
    assert!(!cg.has_succ(1));
  }
  
  #[test]
  fn has_pred() {
    let mut cg = CharacterGraph::new();
    let mut c = Character::new(String::from("test"));
    let mut c2 = Character::new(String::from("test2"));
    cg.add_ch(c);
    cg.add_ch(c2);
    assert!(!cg.has_pred(0));
    cg.add_relation(1, 0);
    assert!(cg.has_pred(0));
    assert!(!cg.has_pred(1));
  }
  
  #[test]
  fn get_succs() {
    let mut cg = CharacterGraph::new();
    let mut c = Character::new(String::from("test"));
    let mut c2 = Character::new(String::from("test2"));
    cg.add_ch(c);
    cg.add_ch(c2);
    assert_eq!(cg.get_succs(0).len(), 0);
    cg.add_relation(0, 1);
    assert_eq!(cg.get_succs(0)[0], 1);
  }
  
  #[test]
  fn get_preds() {
    let mut cg = CharacterGraph::new();
    let mut c = Character::new(String::from("test"));
    let mut c2 = Character::new(String::from("test2"));
    cg.add_ch(c);
    cg.add_ch(c2);
    assert_eq!(cg.get_preds(0).len(), 0);
    cg.add_relation(1, 0);
    assert_eq!(cg.get_preds(0)[0], 1);
  }
}