/**
 * @file Entry point for Electron. Used only when running the
 * desktop app. This file doesn't do much more than what the
 * Electron doc tells you.
 */

/* If the DESKTOP argument is true when starting from command
   line, the program will load the desktop UI, else, it will only
   launch a local server.
*/
if (process.env.npm_config_DESKTOP) {
  const {app, BrowserWindow} = require('electron')
  const url = require('url')
  const path = require('path')

  app.allowRendererProcessReuse = false

  function createWindow() {
    let mainWindow = new BrowserWindow({
      width: 800,
      height: 600,
      webPreferences: {
        nodeIntegration: true
      }
    })
    mainWindow.loadFile('index.html')
  //  mainWindow.setMenu(null)
  }

  app.on('ready', createWindow)
  require('./server')
}
else {
  require('./server')
}