---
layout: default
nav_order: 2
title: Documentation
has_children: true
---

# {{page.title}}

This section regroups everything related to the documentation of the project, most notably 
the [logic module documentation](logic) and the [UI module documentation](ui).

{:toc}