'use strict'
const jsdoc2md = require('jsdoc-to-markdown')
const fs = require('fs')
const path = require('path')

/* output path */
const outputDir = '../pages/documentation/ui'

/* Iterate through files in the src directory */
var files = fs.readdirSync('src/').filter(fn => fn.endsWith('.js'))

/* create a documentation file for each file */
files.forEach(function(file) {
  const templateData = jsdoc2md.getTemplateDataSync({ files: 'src/' + file })

  if(templateData.length != 0) {
    let filename_no_ext = file.toString().substring(0, 
      file.toString().length - 3)
    let title = filename_no_ext.charAt(0).toUpperCase() + filename_no_ext.slice(1)
    const template = 
    `---\nlayout: default\ntitle: ` 
    + title + '\nparent: UI Documentation\ngrand_parent: Documentation\n---\n'
  
    console.log(`rendering ` + file.toString())
    const output = jsdoc2md.renderSync({ data: templateData,
       template: template + '{{>main}}'})
    fs.writeFileSync(path.resolve(outputDir, filename_no_ext + '.md'), output)
  }
})